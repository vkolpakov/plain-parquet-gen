import java.io.File

import io.fastdata.spark.ParquetRowsWriter

import scala.util.Random

object ParquetGen {

  case class Config(rowsInMillions: Int = 0,
                    partitions: Int = 0,
                    outputDir: File = null,
                    compression: Boolean = false)

  val parser = new scopt.OptionParser[Config]("parquet generator") {
    opt[Int]('n', "rows").required().action((x, c) =>
      c.copy(rowsInMillions = x)).text("number of rows in millions")
    opt[Int]('p', "partitions").required().action((x, c) =>
      c.copy(partitions = x)).text("number of partitions")
    opt[File]('o', "output").required().action((x, c) =>
      c.copy(outputDir = x)).text("output dir")
    opt[Boolean]('c', "compression").required().action((x, c) =>
      c.copy(compression = x)).text("enable compression (true|false)")
  }

  val rnd = new Random(0)

  def main(args: Array[String]): Unit = {
    val config = parser.parse(args, Config()).getOrElse(sys.exit())
    val fileSize = config.rowsInMillions * 1000 * 1000 / config.partitions
    if (config.outputDir.exists()) {
      println(s"${config.outputDir} already exists")
      return
    }
    config.outputDir.mkdir()
    for (partition <- 0 until config.partitions) {
      val idFrom = partition * fileSize
      val idTo = partition * fileSize + fileSize
      val file = new File(config.outputDir, s"part_$partition")
      ParquetRowsWriter.createParquetFile(idFrom, idTo, file.getAbsolutePath, config.compression)
      println(file)
    }
  }
}
