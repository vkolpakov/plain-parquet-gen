package io.fastdata.spark;

import org.apache.parquet.io.api.Binary;
import org.apache.parquet.io.api.RecordConsumer;

public class DummyRecordConsumer extends RecordConsumer {
    public void startMessage() {
        System.out.println("startMessage");
    }

    public void endMessage() {
        System.out.println("endMessage");
    }

    public void startField(String field, int index) {
        System.out.println("startField(" + field + ", " + index + ")");
    }

    public void endField(String field, int index) {
        System.out.println("endField(" + field + ", " + index + ")");
    }

    public void startGroup() {

    }

    public void endGroup() {

    }

    public void addInteger(int value) {

    }

    public void addLong(long value) {
        System.out.println("addLong(" + value + ")");
    }

    public void addBoolean(boolean value) {

    }

    public void addBinary(Binary value) {

    }

    public void addFloat(float value) {

    }

    public void addDouble(double value) {

    }
}
