package io.fastdata.spark;

import org.apache.parquet.column.ColumnDescriptor;
import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.column.values.ValuesWriter;
import org.apache.parquet.column.values.factory.ValuesWriterFactory;
import org.apache.parquet.column.values.plain.PlainValuesWriter;
import org.apache.parquet.schema.PrimitiveType;

import static io.fastdata.spark.ParquetRowsWriter.DEFAULT_PAGE_SIZE;

public class PlainLongValuesWriterFactory implements ValuesWriterFactory {
    private ParquetProperties parquetProperties;

    public void initialize(ParquetProperties parquetProperties) {
        this.parquetProperties = parquetProperties;
    }

    public ValuesWriter newValuesWriter(ColumnDescriptor descriptor) {
        if (descriptor.getType().equals(PrimitiveType.PrimitiveTypeName.INT64)) {
            return new PlainValuesWriter(DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE, parquetProperties.getAllocator());
        }
        throw new RuntimeException();
    }
}
