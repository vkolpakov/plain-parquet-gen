/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package io.fastdata.spark;


import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.reflect.ReflectData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroWriteSupport;
import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.column.values.factory.DefaultValuesWriterFactory;
import org.apache.parquet.column.values.factory.ValuesWriterFactory;
import org.apache.parquet.hadoop.*;
import org.apache.parquet.hadoop.api.WriteSupport;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.schema.MessageType;
import org.apache.parquet.schema.PrimitiveType;

import java.io.IOException;
import java.util.Random;

import static org.apache.parquet.schema.PrimitiveType.PrimitiveTypeName.INT64;
import static org.apache.parquet.schema.Type.Repetition.REQUIRED;

public class ParquetRowsWriter {

    public static final int DEFAULT_BLOCK_SIZE = 128 * 1024 * 1024;
    public static final int MAX_PADDING_SIZE_DEFAULT = 8 * 1024 * 1024; // 8MB

    public static final int DEFAULT_PAGE_SIZE = 1024 * 1024;

    static final float DEFAULT_MEMORY_POOL_RATIO = 0.95f;
    static final long DEFAULT_MIN_MEMORY_ALLOCATION = 1 * 1024 * 1024; // 1MB


    static class Row {
        public long id;
        public long randomLong;
        public long smallRandomLong;
    }

    static Random rnd = new Random(0);

    public static void createParquetFile(int idFrom,
                                         int idTo,
                                         String filePath,
                                         boolean useCompression) throws IOException, InterruptedException {
        ParquetRecordWriter<Row> rowParquetRecordWriter;
        if (useCompression) {
            rowParquetRecordWriter = createRowParquetRecordWriter(
                    filePath,
                    new DefaultValuesWriterFactory(),
                    CompressionCodecName.SNAPPY
            );
        } else {
            rowParquetRecordWriter = createRowParquetRecordWriter(
                    filePath,
                    new PlainLongValuesWriterFactory(),
                    CompressionCodecName.UNCOMPRESSED
            );
        }
        Row row = new Row();
        for (int i = idFrom; i < idTo; i++) {
            row.id = i;
            row.randomLong = rnd.nextLong();
            row.smallRandomLong = (long) (10000 * rnd.nextDouble());
            rowParquetRecordWriter.write(null, row);
        }
        rowParquetRecordWriter.close(null);
    }

    private static ParquetRecordWriter<Row> createRowParquetRecordWriter(String filePath,
                                                                         ValuesWriterFactory valuesWriterFactory,
                                                                         CompressionCodecName compressionCodecName)
            throws IOException {
        Configuration conf = new Configuration();
        MessageType messageType = new MessageType("Row",
                new PrimitiveType(REQUIRED, INT64, "id"),
                new PrimitiveType(REQUIRED, INT64, "randomLong"),
                new PrimitiveType(REQUIRED, INT64, "smallRandomLong")
        );
        Schema schema = SchemaBuilder
                .record("Row").namespace("io.fastdata.spark")
                .fields()
                .name("id").type("long").noDefault()
                .name("randomLong").type("long").noDefault()
                .name("smallRandomLong").type("long").noDefault()
                .endRecord();
        AvroWriteSupport<Row> writeSupport = new AvroWriteSupport<Row>(messageType, schema, new ReflectData());
        WriteSupport.WriteContext writeContext = writeSupport.init(conf);
        LocalFileSystem fileSystem = new LocalFileSystem();
        Path path = fileSystem.makeQualified(new Path(filePath));
        ParquetFileWriter w = new ParquetFileWriter(
                conf,
                writeContext.getSchema(),
                path,
                ParquetFileWriter.Mode.CREATE,
                DEFAULT_BLOCK_SIZE,
                MAX_PADDING_SIZE_DEFAULT
        );
        w.start();

        ParquetProperties props = ParquetProperties.builder()
                .withPageSize(DEFAULT_PAGE_SIZE)
                .withDictionaryEncoding(false)
                .withWriterVersion(ParquetProperties.WriterVersion.PARQUET_2_0)
                .withValuesWriterFactory(valuesWriterFactory)
                .build();
        return ParquetRecordWriterFactory.create(
                w,
                writeSupport,
                writeContext.getSchema(),
                writeContext.getExtraMetaData(),
                DEFAULT_BLOCK_SIZE,
                compressionCodecName,
                true,
                props,
                new MemoryManager(DEFAULT_MEMORY_POOL_RATIO, DEFAULT_MIN_MEMORY_ALLOCATION),
                conf
        );
    }

}
