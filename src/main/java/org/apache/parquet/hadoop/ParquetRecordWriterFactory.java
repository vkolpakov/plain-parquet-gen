package org.apache.parquet.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.hadoop.api.WriteSupport;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.schema.MessageType;

import java.util.Map;

public class ParquetRecordWriterFactory {

    public static <T> ParquetRecordWriter<T> create(ParquetFileWriter w,
                                                 WriteSupport<T> writeSupport,
                                                 MessageType schema,
                                                 Map<String, String> extraMetaData,
                                                 long blockSize,
                                                 CompressionCodecName codec,
                                                 boolean validating,
                                                 ParquetProperties props,
                                                 MemoryManager memoryManager,
                                                 Configuration conf) {
        return new ParquetRecordWriter<T>(
                w,
                writeSupport,
                schema,
                extraMetaData,
                blockSize,
                codec,
                validating,
                props,
                memoryManager,
                conf
        );
    }
}
